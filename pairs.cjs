
const pairs = (obj) => {

    if(typeof obj !== 'object' || Array.isArray(obj) || obj == null){
        return [];
    } 
     const keyValue = [];
     for(let key in obj){
        keyValue.push([key,obj[key]]);
     }

return keyValue;

}

module.exports = pairs;