

const defaults = (obj,defaultProps) => {

    if (typeof obj !== 'object' || Array.isArray(obj) || obj == null){
        return [];
    } 

    for(let key in defaultProps){
        const keyPresent =key in obj;
        // console.log(!keyPresent)
        if(!keyPresent){
            obj[key]= defaultProps[key];
        }

    }
    //console.log(obj)
    return obj;
}

module.exports = defaults;
