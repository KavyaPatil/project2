

const mapObject = (obj,cbFunction) => {

 if(typeof obj !== 'object' || Array.isArray(obj) || obj == null){
        return [];
    } 

for(let key in obj){
    obj[key] = cbFunction(obj[key],key);
}

return obj;
}

module.exports = mapObject;